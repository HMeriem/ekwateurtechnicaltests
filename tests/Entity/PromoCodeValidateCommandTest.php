<?php

namespace App\Tests\Util;

use App\Entity\PromoCodeValidateForTestClass;
use PHPUnit\Framework\TestCase;

class PromoCodeValidateCommandTest extends TestCase
{
    /**
     * Check the methods with an inexistant voucher code.
     */
    public function testcheckVoucherExists()
    {
        $promo_code_validate = new PromoCodeValidateForTestClass();
        $result = $promo_code_validate->checkVoucher('acvbn', 'https://601025826c21e10017050013.mockapi.io/ekwatest/promoCodeList', 'https://601025826c21e10017050013.mockapi.io/ekwatest/offerList');

        $this->assertEquals(false, $result);
    }

    /**
     * Test the methods with a bad Url for PromoCodeList.
     */
    public function testcheckVoucherRetrievePromoCodeList()
    {
        $promo_code_validate = new PromoCodeValidateForTestClass();
        $result = $promo_code_validate->checkVoucher('WOODY', 'https://601025826c21e10017050013.mockapi.io/ekwatest/promoCoeList', 'https://601025826c21e10017050013.mockapi.io/ekwatest/offerList');

        $this->assertEquals(false, $result);
    }

    /**
     * Test the methods with good arguments.
     */
    public function testcheckVoucherRetrieveAllApi()
    {
        $promo_code_validate = new PromoCodeValidateForTestClass();
        $result = $promo_code_validate->checkVoucher('WOODY', 'https://601025826c21e10017050013.mockapi.io/ekwatest/promoCodeList', 'https://601025826c21e10017050013.mockapi.io/ekwatest/offerList');

        $this->assertEquals(true, $result);
    }
}