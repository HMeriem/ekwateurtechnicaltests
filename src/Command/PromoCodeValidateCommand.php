<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpClient\HttpClient;

class PromoCodeValidateCommand extends Command
{
    protected static $defaultName = 'promo-code:validate';
    protected static $defaultDescription = 'Test a voucher';

    /**
     * Configuration of the command.
     * 
     * @return integer
     */
    protected function configure()
    {
        //Set description and arguments
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('voucher', InputArgument::REQUIRED, 'Vouchers to test')
        ;
    }

    /**
     * Execute the command.
     * 
     * @param Object $input  Object Input.
     * @param Object $output Object Output.
     * 
     * @return integer
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        //Retrieve the voucher code passed in argument.
        $voucher_code = $input->getArgument('voucher');
        //Check if the voucher code exist.
        if (!$voucher_code) {
            $io->error('No voucher code given in argument');
            return Command::FAILURE;
        }
        //Execute the checkvoucher methods.
        $success = $this->checkVoucher($input, $output, $voucher_code);
        //If the checkvoucher methods is empty or return false the command return a failure code.
        if (!$success) {
            $io->error('The JSON file was not generate');
            return Command::FAILURE;
        }

        $io->success('The JSON file is successfuly generate !');

        return Command::SUCCESS;
    }

    /**
     * Check if all the data are valid and generate the JSON file.
     * 
     * @param Object $input              Object Input.
     * @param Object $output             Object Output.
     * @param String $voucher_to_compare Voucher to compare to given file.
     * 
     * @return boolean|mixed
     */
    public function checkVoucher(InputInterface $input, OutputInterface $output, $voucher_to_compare)
    {
        $filesystem = new Filesystem();
        $io = new SymfonyStyle($input, $output);

        try {
            $filesystem->mkdir(sys_get_temp_dir().'/'.random_int(0, 1000));
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating your directory at ".$exception->getPath();
        }
        //Set date of the day
        $now = new \DateTime('now');
        $voucher_exist = false;
        $voucher_end_date = false;
        //Retrieve the promo code list datas.
        $promos_code_list = $this->getPromoCodeList($input, $output);
        //Check the datas.
        if (empty($promos_code_list)) {
            $io->error('Empty data PromoCodeList');
            return false;
        }

        foreach ($promos_code_list as $promo_code_list) {
            //Check if the code given by the user are present in the promo code list.
            if ($promo_code_list['code'] == $voucher_to_compare) {
                //Set the variable who confirm that the data exist to true. 
                $voucher_exist = true;
                //Stock datas for end date and discount value. And check if the voucher are not expired.
                if ($promo_code_list['endDate'] > $now->format('Y-m-d')) {
                    $voucher_end_date = $promo_code_list['endDate'];
                    $voucher_discount_value = $promo_code_list['discountValue'];
                    break;
                }
            }
        }
        //If the voucher code did'nt exist in the code promo list, we return false.
        if (!$voucher_exist) {
            $io->error('Voucher do not exist');
            return false;
        }
        //If the value of end date are false, then the voucher is expired.
        if (!$voucher_end_date) {
            $io->error('Voucher is expire');
            return false;
        }
        //Retrieve the offerList Json
        $offers_list = $this->getOfferList($input, $output);
        if (empty($promos_code_list)) {
            $io->error('Empty data OfferList');
            return false;
        }

        //Set the array for the creation of the JSON file
        $res = [];
        $res['promoCode'] = $voucher_to_compare;
        $res['endDate'] = $voucher_end_date;
        $res['discountValue'] = floatval($voucher_discount_value);
        //Set the values for the array
        foreach ($offers_list as $offer_list) {
            foreach ($offer_list['validPromoCodeList'] as $valid_promo_code_list) {
                if ($valid_promo_code_list == $voucher_to_compare) {
                    $compatibleOfferList = [];
                    $compatibleOfferList['name'] = $offer_list['offerName'];
                    $compatibleOfferList['type'] = $offer_list['offerType'];
                    $res['compatibleOfferList'][] = $compatibleOfferList;
                }
            }
        }
        //Create the json file
        $filesystem->dumpFile('./bin/compatibleVoucher.json', json_encode($res));
        //Check if the Json file exists
        if (!$filesystem->exists('./bin/compatibleVoucher.json')) {
            $io->error('The json file was not created');
            return false;
        }

        return true;
    }

    /**
     * Retrieve offerList by API Call.
     * 
     * @param Object $input  Object Input.
     * @param Object $output Object Output.
     * 
     * @return boolean|array
     */
    protected function getOfferList(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        //Create the httpclient
        $client = HttpClient::create();
        $response = $client->request('GET', 'https://601025826c21e10017050013.mockapi.io/ekwatest/offerList');
        $statusCode = $response->getStatusCode();
        //Check if the statut code is ok
        if ($statusCode != 200) {
            $io->error('The required data OfferList is inaccessible');
            return false;
        }
        //Retrieve the array content
        $content = $response->toArray();

        return $content;
    }

    /**
     * Retrieve offerList by API Call.
     * 
     * @param Object $input  Object Input.
     * @param Object $output Object Output.
     * 
     * @return boolean|array
     */
    protected function getPromoCodeList(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        //Create the httpclient
        $client = HttpClient::create();
        $response = $client->request('GET', 'https://601025826c21e10017050013.mockapi.io/ekwatest/promoCodeList');
        $statusCode = $response->getStatusCode();
        //Check if the statut code is ok
        if ($statusCode != 200) {
            $io->error('The required data PromoCodeList is inaccessible');
            return false;
        }
        //Retrieve the array content
        $content = $response->toArray();

        return $content;
    }
}
