<?php

namespace App\Entity;

use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpClient\HttpClient;

//Entity create for the unit test. They don't take in charge input and ouput. Plus we can now test the URL for the API
class PromoCodeValidateForTestClass
{
    /**
     * Check if all the data are valid and generate the JSON file.
     * 
     * @param String $voucher_to_compare  Voucher to compare to given file.
     * @param String $url_promo_code_list Url for promo code list.
     * @param String $url_offer_list      Url for code list.
     * 
     * @return boolean|mixed
     */
    public function checkVoucher($voucher_to_compare, $url_promo_code_list, $url_offer_list)
    {        
        $filesystem = new Filesystem();

        try {
            $filesystem->mkdir(sys_get_temp_dir().'/'.random_int(0, 1000));
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating your directory at ".$exception->getPath();
        }
        //Set date of the day
        $now = new \DateTime('now');
        $voucher_exist = false;
        $voucher_end_date = false;
        //Retrieve the promo code list datas.
        $promos_code_list = $this->getPromoCodeList($url_promo_code_list);
        //Check the datas.
        if (empty($promos_code_list)) {
            return false;
        }

        foreach ($promos_code_list as $promo_code_list) {
            //Check if the code given by the user are present in the promo code list.
            if ($promo_code_list['code'] == $voucher_to_compare) {
                //Set the variable who confirm that the data exist to true. 
                $voucher_exist = true;
                //Stock datas for end date and discount value. And check if the voucher are not expired.
                if ($promo_code_list['endDate'] > $now->format('Y-m-d')) {
                    $voucher_end_date = $promo_code_list['endDate'];
                    $voucher_discount_value = $promo_code_list['discountValue'];
                    break;
                }
            }
        }
        //If the voucher code did'nt exist in the code promo list, we return false.
        if (!$voucher_exist) {
            return false;
        }
        //If the value of end date are false, then the voucher is expired.
        if (!$voucher_end_date) {
            return false;
        }
        //Retrieve the offerList Json
        $offers_list = $this->getOfferList($url_offer_list);
        if (empty($promos_code_list)) {
            return false;
        }

        //Set the array for the creation of the JSON file
        $res = [];
        $res['promoCode'] = $voucher_to_compare;
        $res['endDate'] = $voucher_end_date;
        $res['discountValue'] = intval($voucher_discount_value);
        //Set the values for the array
        foreach ($offers_list as $offer_list) {
            foreach ($offer_list['validPromoCodeList'] as $valid_promo_code_list) {
                if ($valid_promo_code_list == $voucher_to_compare) {
                    $compatibleOfferList = [];
                    $compatibleOfferList['name'] = $offer_list['offerName'];
                    $compatibleOfferList['type'] = $offer_list['offerType'];
                    $res['compatibleOfferList'][] = $compatibleOfferList;
                }
            }
        }
        //Create the json file
        $filesystem->dumpFile('./bin/compatibleVoucher.json', json_encode($res));
        //Check if the Json file exists
        if (!$filesystem->exists('./bin/compatibleVoucher.json')) {
            return false;
        }

        return true;
    }

    /**
     * Retrieve offerList by API Call.
     * 
     * 
     * @return boolean|array
     */
    protected function getOfferList($url)
    {
        //Create the httpclient
        $client = HttpClient::create();
        $response = $client->request('GET', $url);
        $statusCode = $response->getStatusCode();
        //Check if the statut code is ok
        if ($statusCode != 200) {
            return false;
        }
        //Retrieve the array content
        $content = $response->toArray();

        return $content;
    }

    /**
     * Retrieve offerList by API Call.
     * 
     * 
     * @return boolean|array
     */
    protected function getPromoCodeList($url)
    {
        //Create the httpclient
        $client = HttpClient::create();
        $response = $client->request('GET', $url);
        $statusCode = $response->getStatusCode();
        //Check if the statut code is ok
        if ($statusCode != 200) {
            return false;
        }
        //Retrieve the array content
        $content = $response->toArray();

        return $content;
    }
}